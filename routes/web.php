<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('prefix/{name}', function($name){

	return "Helo world";

});


/*
Isang Module isang Group
Prefix = yan ung type mo sa browser na link, parent sya ng index,create,delete
sample:
localhost:8000/student = Prefix
localhost:8000/student/create


*/ 

Route::group(['as'=>"student." ,'prefix'=>"student"],function(){


// eto ung sundan mo 
// meron
	Route::get('',['as'=>"index",'uses'=>"StudentController@index"]);
	Route::get('edit',['as'=>"edit",'uses'=>"StudentController@edit"]);
	Route::get('create',['as'=> "create",'uses'=>"StudentController@create"]);

});